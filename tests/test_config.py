import os
from search.config import Config


class TestConfig(Config):
    def __init__(self, auth_root: str, nlp_url: str) -> None:
        current_dir = os.path.dirname(__file__)

        Config.CONFIG_APP_FILE = os.path.join(current_dir, "../config/search.conf")
        Config.CONFIG_LOGS_FILE = os.path.join(current_dir, "../config/logs.conf")

        super().__init__(section_name="Test")

        TestConfig.JWT_TOKEN_AUTH_URL = f"http://{auth_root}/authentication/v1/token"
        TestConfig.NLP_API_ROOT_URL = nlp_url
        TestConfig.INTERNAL_AUTHORIZATION_API = f"http://{auth_root}/authorization/v1/"

        TestConfig.DB_HOST = os.environ.get("DB_HOST")
        TestConfig.DB_NAME = os.environ.get("DB_NAME")
        TestConfig.DB_PORT = 5432

        port = os.environ.get("DB_PORT")
        if port:
            TestConfig.DB_PORT = int(port)

        TestConfig.DB_USER = os.environ.get("DB_USER")
        TestConfig.DB_PASSWORD = os.environ.get("DB_PASSWORD")

        TestConfig.RAG_SECRET_KEY = os.environ.get("RAG_SECRET_KEY")
