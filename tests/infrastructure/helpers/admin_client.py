import urllib.parse as urllib_parse
import requests


def create_index(root_url: str, index_name: str):
    """Create index using internal admin endpoint."""

    url = urllib_parse.urljoin(root_url, "indexes")

    payload = {"name": index_name}

    result = requests.post(
        url=url, json=payload, headers={"Content-Type": "application/json"}, timeout=30
    )
    result.raise_for_status()
