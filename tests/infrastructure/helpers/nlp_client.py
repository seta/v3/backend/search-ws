from search.infrastructure.clients import nlp_client


def compute_embeddings(text: str, nlp_url: str) -> list[list[float]]:
    """Compute embeddings for the text."""
    embedding_list = []

    embeddings = nlp_client.compute_embeddings(text=text, api_root_url=nlp_url)

    assert "emb_with_chunk_text" in embeddings
    assert len(embeddings["emb_with_chunk_text"]) > 0

    for emb in embeddings["emb_with_chunk_text"]:
        assert "vector" in emb
        embedding_list.append(emb["vector"])

    return embedding_list
