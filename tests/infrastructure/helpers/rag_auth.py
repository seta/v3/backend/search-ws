from datetime import datetime
import pytz

from flask_jwt_extended import tokens
from flask_jwt_extended.config import config as jwt_config


def create_rag_token(user_id: str, secret: str, expires: datetime | None = None) -> str:
    """Encode RAG token"""

    if expires is None:
        expires = datetime.now(pytz.utc).replace(hour=23, minute=59, second=59)

    now = datetime.now(pytz.utc)
    expires_delta = expires - now
    identity = {"user_id": user_id, "provider": "SETA", "provider_uid": user_id}
    claim_overrides = {"iss": "SETA Auth Server", "sub": identity["user_id"]}

    # pylint: disable-next=protected-access
    access_token = tokens._encode_jwt(
        algorithm=jwt_config.algorithm,
        audience=jwt_config.encode_audience,
        claim_overrides=claim_overrides,
        csrf=False,
        expires_delta=expires_delta,
        fresh=None,
        header_overrides=None,
        identity=identity,
        identity_claim_key=jwt_config.identity_claim_key,
        issuer=jwt_config.encode_issuer,
        json_encoder=jwt_config.json_encoder,
        secret=secret,
        token_type="access",
        nbf=jwt_config.encode_nbf,
    )

    return access_token
