import datetime
import pytz

import sqlalchemy as db

from tests.infrastructure.helpers.util import get_public_key
from tests.infrastructure.helpers.data_loader import load_users_data, load_data_sources


class PostgDBConnection:

    def __init__(
        self, db_host: str, db_port: int, db_name: str, db_user: str, db_pass: str
    ) -> None:

        self.engine = db.create_engine(
            f"postgresql://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}"
        )


class PostgDbTest:

    def __init__(self, db_connection: PostgDBConnection, user_key_pairs: dict) -> None:
        self.db_connection = db_connection
        self.user_key_pairs = user_key_pairs

    def init_db(self) -> None:
        """Initialize test database and its collections."""

        self._populate_tables()

    def clear_db(self) -> None:
        """Clear all tables from the database."""

        with self.db_connection.engine.connect() as connection:
            with connection.begin():

                all_tables = [
                    "session_tokens",
                    "data_source_scopes",
                    "system_scopes",
                    "stored_searches",
                    "sessions",
                    "rsa_key",
                    "providers",
                    "profile_unsearchables",
                    "library",
                    "data_sources",
                    "claims",
                    "applications",
                    "users",
                    "search_indexes",
                    "catalogue_scopes",
                    "catalogue_roles",
                    "catalogue_fields",
                    "catalogue_discovery_domains",
                    "annotations",
                ]

                for table in all_tables:
                    connection.execute(db.text(f"DELETE FROM public.{table}"))

    def _populate_tables(self) -> None:
        with self.db_connection.engine.connect() as connection:
            with connection.begin():

                now_date = datetime.datetime.now(tz=pytz.utc)

                data = load_users_data()

                # save users
                for user in data["users"]:
                    connection.execute(
                        statement=db.text(
                            """
                            insert into public.users (user_id, email, user_type, status, created_at)
                            values (:user_id, :email, :user_type, :status, :created_at)
                            """
                        ),
                        parameters=dict(
                            user_id=user["user_id"],
                            email=user["email"],
                            user_type=user["user_type"],
                            status=user["status"],
                            created_at=now_date,
                        ),
                    )

                    # insert public key
                    pub_key = get_public_key(user["user_id"], self.user_key_pairs)

                    if pub_key:
                        connection.execute(
                            statement=db.text(
                                """
                            insert into public.rsa_key (user_id, rsa_value, created_at)
                            values (:user_id, :rsa_value, :created_at)
                            """
                            ),
                            parameters={
                                "user_id": user["user_id"],
                                "rsa_value": pub_key,
                                "created_at": now_date,
                            },
                        )

                # save user providers
                for provider in data["providers"]:
                    connection.execute(
                        statement=db.text(
                            """
                            insert into public.providers (user_id, provider_uid, provider, first_name, last_name, domain)
                            values (:user_id, :provider_uid, :provider, :first_name, :last_name, :domain)
                            """
                        ),
                        parameters={
                            "user_id": provider["user_id"],
                            "provider_uid": provider["provider_uid"],
                            "provider": provider["provider"],
                            "first_name": provider["first_name"],
                            "last_name": provider["last_name"],
                            "domain": provider["domain"],
                        },
                    )

                # save user claims
                for claim in data["claims"]:
                    connection.execute(
                        statement=db.text(
                            """
                        insert into public.claims (user_id, claim_type, claim_value)
                        values (:user_id, :claim_type, :claim_value)
                        """
                        ),
                        parameters={
                            "user_id": claim["user_id"],
                            "claim_type": claim["claim_type"],
                            "claim_value": claim["claim_value"],
                        },
                    )

                # Load resources:
                data = load_data_sources()

                if "dataSources" in data:
                    for data_source in data["dataSources"]:
                        connection.execute(
                            db.text(
                                """
                            insert into public.data_sources (id, title, description, index_name, organisation, themes, 
                                status, contact_person, contact_email, contact_website, creator_id, created_at, discovery_domains)
                            values (:id, :title, :description, :index_name, :organisation, :themes, 
                                :status, :contact_person, :contact_email, :contact_website, :creator_id, :created_at, :discovery_domains)
                            """
                            ),
                            parameters={
                                "id": data_source["data_source_id"],
                                "title": data_source["title"],
                                "description": data_source["description"],
                                "index_name": data_source["index_name"],
                                "organisation": data_source["organisation"],
                                "themes": data_source["themes"],
                                "status": data_source["status"],
                                "contact_person": data_source["contact"]["person"],
                                "contact_email": data_source["contact"]["email"],
                                "contact_website": data_source["contact"]["website"],
                                "creator_id": data_source["creator_id"],
                                "created_at": now_date,
                                "discovery_domains": data_source.get(
                                    "discovery_domains", None
                                ),
                            },
                        )

                # save data source scopes
                if "scopes" in data:
                    for scope in data["scopes"]:
                        connection.execute(
                            statement=db.text(
                                """
                            insert into public.data_source_scopes (user_id, data_source_id, scope)
                            values (:user_id, :data_source_id, :scope)
                            """
                            ),
                            parameters={
                                "user_id": scope["user_id"],
                                "data_source_id": scope["data_source_id"],
                                "scope": scope["scope"],
                            },
                        )
