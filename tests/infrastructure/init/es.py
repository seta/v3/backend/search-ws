from datetime import datetime as dt
import json
from pathlib import Path
from opensearchpy import OpenSearch

from flask import current_app

from search.apis.corpus.corpus_logic import insert_doc
from tests.infrastructure.helpers import admin_client


class SetaES:
    def __init__(self, host: str, indexes: list[str], admin_url: str) -> None:
        self.indexes = indexes
        self.es = OpenSearch("http://" + host, verify_certs=False, request_timeout=30)
        self.admin_url = admin_url

    def init_es(self) -> None:
        """Add documents to ElasticSearch."""

        cnt_docs = 0

        base_path = Path(__file__).parent
        data_path = "../data/documents"

        root_path = (base_path / data_path).resolve()

        for file in root_path.iterdir():
            if file.is_file():
                self._insert_doc(file)

                cnt_docs += 1

        print("ES insert " + str(cnt_docs) + " documents.")

    def create_indexes(self) -> None:
        """Create indexes."""

        try:
            for index in self.indexes:
                if not self.index_exists(index):
                    admin_client.create_index(self.admin_url, index)
        except Exception as e:
            print(e)
            raise

    def index_exists(self, index: str) -> bool:
        """Check if index exists."""

        return self.es.indices.exists(index=index)

    def refresh_indexes(self) -> None:
        """Refresh indexes."""

        try:
            for index in self.indexes:
                self.es.indices.refresh(index=index)
        except Exception as e:
            print(e)
            raise

    def cleanup(self) -> None:
        """Delete all documents from the test indexes."""

        try:
            for index in self.indexes:
                if self.index_exists(index):
                    self.es.indices.delete(index=index)
        except Exception as e:
            print(e)

    def _insert_doc(self, file_path: Path) -> None:
        with open(file_path, encoding="utf-8") as fp:
            data = json.load(fp)
            data["date"] = dt.strptime(data["date"], "%Y-%m-%d")

            index = data["index"]
            del data["index"]

            insert_doc(args=data, es=self.es, index=index, current_app=current_app)
