## Seta-Search Testing

Seta-Search is using pytest for functional and unit testing.

### Usage
    For testing in docker run: 
    pytest -s tests/ --settings=REMOTE

### Configuration

Modify the *test.conf* file to suit your testing environment.

Create a file with name *secrets.conf* for [configparser](https://docs.python.org/3.10/library/configparser.html) that looks like this:

```
[TEST]
# same secret as in .env.test
API_SECRET_KEY= 
RAG_SECRET_KEY= 
```
Note: This file name is set to be ignored by git in *.gitignore*.

The configuration file is read by [ConfigParser](https://docs.python.org/3.10/library/configparser.html) in *conftest.py*, *init_os* fixture.