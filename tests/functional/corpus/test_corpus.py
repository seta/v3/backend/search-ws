from http import HTTPStatus
import pytest
from flask.testing import FlaskClient

from tests.infrastructure.helpers.authentication import login_user
from tests.infrastructure.helpers.util import get_access_token
from tests.infrastructure.init.es import SetaES

from tests.functional.corpus import api


@pytest.mark.parametrize("user_id", ["seta_admin"])
@pytest.mark.parametrize("source", ["test1"])
def test_corpus_doc(
    client: FlaskClient,
    search_engine: SetaES,
    user_key_pairs: dict,
    user_id: str,
    source: str,
):
    """
    Add a new document, get its contents and delete it
    """
    authentication_url = client.application.config["JWT_TOKEN_AUTH_URL"]
    response = login_user(
        auth_url=authentication_url, user_key_pairs=user_key_pairs, user_id=user_id
    )
    access_token = get_access_token(response)
    article_id = "test1:article:2"

    data = {
        "source": source,
        "id": article_id,
        "title": "Evaluation of policy options to deal with the greenhouse effect",
    }

    response = api.add_document(client=client, access_token=access_token, data=data)
    assert response.status_code == HTTPStatus.OK
    assert "_id" in response.json
    doc_id = response.json["_id"]

    search_engine.refresh_indexes()

    response = api.get_document(
        client=client, access_token=access_token, document_id=doc_id, source=source
    )
    assert response.status_code == HTTPStatus.OK
    assert "chunk_list" in response.json
    assert "source" in response.json["chunk_list"][0]
    assert response.json["chunk_list"][0]["source"] == source
    assert "id" in response.json["chunk_list"][0]
    assert response.json["chunk_list"][0]["id"] == article_id
    assert "title" in response.json["chunk_list"][0]
    assert (
        response.json["chunk_list"][0]["title"]
        == "Evaluation of policy options to deal with the greenhouse effect"
    )

    response = api.delete_document(
        client=client, access_token=access_token, document_id=doc_id, source=source
    )
    assert response.status_code == HTTPStatus.OK

    search_engine.refresh_indexes()

    response = api.get_document(
        client=client, access_token=access_token, document_id=doc_id, source=source
    )
    assert response.status_code == HTTPStatus.OK
    assert "chunk_list" in response.json
    assert len(response.json["chunk_list"]) == 0
