import time
from flask.testing import FlaskClient

from tests.infrastructure.helpers.util import auth_headers

API_ROOT = "/seta-search/api/v1/corpus"


def add_document(client: FlaskClient, access_token: str, data):
    """Insert document into search engine."""

    url = f"{API_ROOT}/document"
    resp = client.post(
        url,
        json=data,
        content_type="application/json",
        headers=auth_headers(access_token),
    )

    print("Wait for 5 second after put a document", flush=True)
    time.sleep(5)
    return resp


def get_document(client: FlaskClient, access_token: str, document_id: str, source: str):
    """Get document chunks."""

    url = f"{API_ROOT}/document/id"
    data = {"document_id": document_id, "source": source}

    return client.post(
        url,
        json=data,
        content_type="application/json",
        headers=auth_headers(access_token),
    )


def delete_document(
    client: FlaskClient, access_token: str, document_id: str, source: str
):
    """Delete document from search engine."""

    url = f"{API_ROOT}/document/id"
    data = {"document_id": document_id, "source": source}
    resp = client.delete(
        url,
        json=data,
        content_type="application/json",
        headers=auth_headers(access_token),
    )
    print("Wait for 5 second after put a document", flush=True)
    time.sleep(5)
    return resp


def post_by_json(client: FlaskClient, access_token: str, json_param: dict):
    """Search for documents"""

    url = API_ROOT

    return client.post(
        url,
        json=json_param,
        content_type="application/json",
        headers=auth_headers(access_token),
    )


def compute_aggregations(client: FlaskClient, access_token: str, json_param: dict):
    """Compute aggregations"""

    url = f"{API_ROOT}/aggregations"

    return client.post(
        url,
        json=json_param,
        content_type="application/json",
        headers=auth_headers(access_token),
    )
