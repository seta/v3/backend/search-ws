from http import HTTPStatus
import pytest
from flask.testing import FlaskClient

from tests.infrastructure.helpers.authentication import login_user
from tests.infrastructure.helpers.util import get_access_token
from tests.infrastructure.init.es import SetaES

from tests.functional.corpus import api


@pytest.mark.parametrize("user_id, aggs", [("seta_admin", "taxonomies")])
def test_corpus_taxonomy_aggregation(
    client: FlaskClient,
    search_engine: SetaES,
    user_key_pairs: dict,
    user_id: str,
    aggs: str,
):
    """Run search with aggregation on taxonomy field for GET corpus methods"""

    authentication_url = client.application.config["JWT_TOKEN_AUTH_URL"]
    response = login_user(
        auth_url=authentication_url, user_key_pairs=user_key_pairs, user_id=user_id
    )
    access_token = get_access_token(response)

    data1 = {
        "source": "test1",
        "id": "test1:article:1",
        "title": "test_corpus_taxonomy_aggregation",
        "date": "2023-01-01",
        "taxonomy": ["eurovocTree:100163", "eurovocTree:883"],
    }

    data2 = {
        "source": "test1",
        "id": "test1:article:1",
        "title": "test_corpus_taxonomy_aggregation",
        "date": "2023-01-01",
        "taxonomy": ["eurovocTree:1", "eurovocTree:2"],
    }

    response = api.add_document(client=client, access_token=access_token, data=data1)
    assert response.status_code == HTTPStatus.OK
    response = api.add_document(client=client, access_token=access_token, data=data2)
    assert response.status_code == HTTPStatus.OK

    search_engine.refresh_indexes()

    json_param = {"aggs": [aggs]}
    response = api.compute_aggregations(
        client=client, access_token=access_token, json_param=json_param
    )
    assert response.status_code == HTTPStatus.OK
    assert "aggregations" in response.json
    assert "taxonomies" in response.json["aggregations"]
    assert len(response.json["aggregations"]["taxonomies"]) > 1
