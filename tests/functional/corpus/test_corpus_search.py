from http import HTTPStatus
import pytest
from flask.testing import FlaskClient


from tests.infrastructure.helpers.authentication import login_user
from tests.infrastructure.helpers.util import get_access_token
from tests.infrastructure.helpers import nlp_client
from tests.infrastructure.init.es import SetaES

from tests.functional.corpus import api


@pytest.mark.parametrize("user_id, term", [("seta_admin", "test_corpus_simple_search")])
def test_corpus_simple_search(
    client: FlaskClient,
    search_engine: SetaES,
    user_key_pairs: dict,
    user_id: str,
    term: str,
):
    """Run simple search for GET & POST corpus methods"""

    authentication_url = client.application.config["JWT_TOKEN_AUTH_URL"]
    response = login_user(
        auth_url=authentication_url, user_key_pairs=user_key_pairs, user_id=user_id
    )
    access_token = get_access_token(response)

    data = {
        "source": "test1",
        "id": "test1:article:1",
        "title": "test_corpus_simple_search",
        "date": "2023-01-01",
    }

    response = api.add_document(client=client, access_token=access_token, data=data)
    assert response.status_code == HTTPStatus.OK
    assert "_id" in response.json
    doc_id = response.json["_id"]

    print(f"Document added with _id={doc_id}", flush=True)

    search_engine.refresh_indexes()

    json_p = {"term": term, "source": ["test1"]}
    response = api.post_by_json(
        client=client, access_token=access_token, json_param=json_p
    )
    assert response.status_code == HTTPStatus.OK
    assert "total_docs" in response.json
    assert response.json["total_docs"] >= 1

    found = False
    for doc in response.json["documents"]:
        if doc["_id"] == doc_id:
            found = True
            break

    assert found


@pytest.mark.parametrize("user_id", ["seta_admin"])
@pytest.mark.parametrize(
    "term", ["Council Regulation (EU) 2016/1686 of 20 September 2016"]
)
def test_duplicates(
    client: FlaskClient,
    user_key_pairs: dict,
    user_id: str,
    term: str,
):
    """Test search by duplicate document id"""

    authentication_url = client.application.config["JWT_TOKEN_AUTH_URL"]
    response = login_user(
        auth_url=authentication_url, user_key_pairs=user_key_pairs, user_id=user_id
    )
    access_token = get_access_token(response)

    json_p = {"term": term}
    response = api.post_by_json(
        client=client, access_token=access_token, json_param=json_p
    )

    assert response.status_code == HTTPStatus.OK
    assert "total_docs" in response.json
    assert response.json["total_docs"] >= 2

    sources = []
    for doc in response.json["documents"]:
        sources.append(doc["source"])
    sources = set(sources)

    assert len(sources) > 1


@pytest.mark.parametrize("user_id", ["seta_admin"])
def test_corpus_taxonomy_search(
    client: FlaskClient, search_engine: SetaES, user_key_pairs: dict, user_id: str
):
    """Run taxonomy search for POST corpus methods"""

    authentication_url = client.application.config["JWT_TOKEN_AUTH_URL"]
    response = login_user(
        auth_url=authentication_url, user_key_pairs=user_key_pairs, user_id=user_id
    )
    access_token = get_access_token(response)

    data1 = {
        "source": "test1",
        "id": "test1:article:1",
        "title": "test_corpus_taxonomy_search",
        "date": "2023-01-01",
        "taxonomy": ["eurovocTree:100163", "eurovocTree:883"],
    }

    data2 = {
        "source": "test1",
        "id": "test1:article:2",
        "date": "2023-01-01",
        "title": "test_corpus_taxonomy_search",
        "taxonomy": ["eurovocTree:1", "eurovocTree:2"],
    }

    response = api.add_document(client=client, access_token=access_token, data=data1)
    assert response.status_code == HTTPStatus.OK
    assert "_id" in response.json
    doc_id_1 = response.json["_id"]

    response = api.add_document(client=client, access_token=access_token, data=data2)
    assert response.status_code == HTTPStatus.OK
    assert "_id" in response.json
    doc_id_2 = response.json["_id"]

    search_engine.refresh_indexes()

    json_p = {
        "term": "test_corpus_taxonomy_search",
        "taxonomy": ["eurovocTree:100163", "eurovocTree:883"],
    }
    response = api.post_by_json(
        client=client, access_token=access_token, json_param=json_p
    )
    assert response.status_code == HTTPStatus.OK
    assert "documents" in response.json
    assert len(response.json["documents"]) == 1
    assert doc_id_1 in response.json["documents"][0]["_id"]

    json_p = {
        "term": "test_corpus_taxonomy_search",
        "taxonomy": ["eurovocTree:11", "eurovocTree:22"],
    }
    response = api.post_by_json(
        client=client, access_token=access_token, json_param=json_p
    )
    assert response.status_code == HTTPStatus.OK
    assert "documents" in response.json
    assert len(response.json["documents"]) == 0

    json_p = {
        "term": "test_corpus_taxonomy_search",
        "taxonomy": ["eurovocTree:1", "eurovocTree:2"],
    }
    response = api.post_by_json(
        client=client, access_token=access_token, json_param=json_p
    )
    assert response.status_code == HTTPStatus.OK
    assert "documents" in response.json
    assert len(response.json["documents"]) == 1
    assert doc_id_2 in response.json["documents"][0]["_id"]


@pytest.mark.parametrize(
    "user_id, payload, expect",
    [
        (
            "seta_admin",
            {
                "term": "strategies",
                "n_docs": 10,
                "from_doc": 0,
                "source": ["test1", "test2"],
                "search_type": "CHUNK_SEARCH",
                "sort": ["date:desc"],
            },
            HTTPStatus.OK,
        ),
        (
            "seta_admin",
            {
                "term": "strategies",
                "n_docs": 10,
                "from_doc": 0,
                "source": ["test1", "test2"],
                "search_type": "DOCUMENT_SEARCH",
                "reference": ["test1:article:1"],
                "collection": ["news;News"],
                "taxonomy": ["eurovocTree:100163", "eurovocTree:883"],
                "in_force": "true",
                "semantic_sort_id_list": ["test1:article:1", "test1:article:2"],
                "author": ["John Doe"],
                "date_range": ["gte:1990-01-01", "lte:2023-12-31"],
                "other": {"crc": "1111111"},
                "annotation": ["ReportingObligations:ROGeneral"],
                "sort": ["document_id:asc"],
                "text_attached": "strategies",
            },
            HTTPStatus.OK,
        ),
        (
            "seta_admin",
            {
                "term": "strategies",
                "n_docs": 10,
                "from_doc": 0,
                "source": ["test1", "test2"],
                "search_type": "CHUNK_SEARCH",
                "sort": "date:desc",
            },
            HTTPStatus.BAD_REQUEST,
        ),
    ],
)
def test_corpus_search(
    client: FlaskClient,
    user_key_pairs: dict,
    user_id: str,
    payload: dict,
    expect: int,
):
    """Run complex search for POST corpus methods"""

    authentication_url = client.application.config["JWT_TOKEN_AUTH_URL"]
    response = login_user(
        auth_url=authentication_url, user_key_pairs=user_key_pairs, user_id=user_id
    )
    access_token = get_access_token(response)

    if "text_attached" in payload:
        nlp_root_url = client.application.config.get("NLP_API_ROOT_URL")
        payload["sbert_embedding_list"] = nlp_client.compute_embeddings(
            text=payload["text_attached"], nlp_url=nlp_root_url
        )

        del payload["text_attached"]

    response = api.post_by_json(
        client=client, access_token=access_token, json_param=payload
    )
    assert response.status_code == expect

    if expect == HTTPStatus.OK:
        assert "total_docs" in response.json
