from http import HTTPStatus
from flask.testing import FlaskClient

from tests.functional.rag import api


def test_info(client: FlaskClient):
    """
    Test most similar terms
    """

    response = api.get_info(client=client)
    assert response.status_code == HTTPStatus.OK
    assert "serviceName" in response.json
    assert "serviceDescription" in response.json
    assert "version" in response.json
    assert "status" in response.json
    assert "uptime" in response.json
    assert "timestamp" in response.json
