from http import HTTPStatus
import pytest
from flask.testing import FlaskClient

from tests.functional.rag import api

from tests.infrastructure.helpers import rag_auth


@pytest.mark.parametrize("user_id", ["seta_admin"])
@pytest.mark.parametrize("query", ["council"])
def test_simple_query(client: FlaskClient, user_id: str, query: str):
    """
    Test simple rag query
    """

    rag_secret_key = client.application.config["RAG_SECRET_KEY"]
    access_token = rag_auth.create_rag_token(user_id=user_id, secret=rag_secret_key)

    response = api.query(client=client, access_token=access_token, rag_query=query)
    assert response.status_code == HTTPStatus.OK

    json_data = response.json
    assert "documents" in json_data

    if len(json_data["documents"]) > 0:
        doc = json_data["documents"][0]
        assert "document_content" in doc
        assert "score" in doc
        assert "metadata" in doc

        assert "id" in doc["metadata"]
        assert "title" in doc["metadata"]
        assert "abstract" in doc["metadata"]
        assert "date" in doc["metadata"]


@pytest.mark.parametrize("user_id", ["seta_admin"])
@pytest.mark.parametrize("query", ["council"])
def test_query_with_collection(client: FlaskClient, user_id: str, query: str):
    """
    Test simple rag query
    """

    rag_secret_key = client.application.config["RAG_SECRET_KEY"]
    access_token = rag_auth.create_rag_token(user_id=user_id, secret=rag_secret_key)

    response = api.get_collections(client=client, access_token=access_token)
    assert response.status_code == HTTPStatus.OK

    json_data = response.json
    assert "collections" in json_data
    assert len(json_data["collections"]) > 0

    response = api.query(
        client=client,
        access_token=access_token,
        rag_query=query,
        collections=json_data["collections"],
    )
    assert response.status_code == HTTPStatus.OK

    json_data = response.json
    assert "documents" in json_data

    if len(json_data["documents"]) > 0:
        doc = json_data["documents"][0]
        assert "document_content" in doc
        assert "score" in doc
        assert "metadata" in doc


@pytest.mark.parametrize("user_id", ["seta_admin"])
@pytest.mark.parametrize(
    "query, collections, filters",
    [
        (
            "council",
            ["Consolidated acts"],
            [
                {"key": "in_force", "value": "false"},
                {"key": "date", "value": ["gte:2020", "lte:2025"]},
            ],
        )
    ],
)
def test_query_with_params(
    client: FlaskClient,
    user_id: str,
    query: str,
    collections: list[str],
    filters: list[dict],
):
    """
    Test simple rag query
    """

    rag_secret_key = client.application.config["RAG_SECRET_KEY"]
    access_token = rag_auth.create_rag_token(user_id=user_id, secret=rag_secret_key)

    response = api.query(
        client=client,
        access_token=access_token,
        rag_query=query,
        collections=collections,
        filters=filters,
    )
    assert response.status_code == HTTPStatus.OK

    json_data = response.json
    assert "documents" in json_data

    if len(json_data["documents"]) > 0:
        doc = json_data["documents"][0]
        assert "document_content" in doc
        assert "score" in doc
        assert "metadata" in doc
