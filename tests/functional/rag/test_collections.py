from http import HTTPStatus
import pytest
from flask.testing import FlaskClient

from tests.functional.rag import api

from tests.infrastructure.helpers import rag_auth


@pytest.mark.parametrize("user_id", ["seta_admin"])
def test_get_collections(client: FlaskClient, user_id: str):
    """
    Test get collections
    """

    rag_secret_key = client.application.config["RAG_SECRET_KEY"]
    access_token = rag_auth.create_rag_token(user_id=user_id, secret=rag_secret_key)

    response = api.get_collections(client=client, access_token=access_token)
    assert response.status_code == HTTPStatus.OK
    assert "collections" in response.json
    assert len(response.json["collections"]) > 0
