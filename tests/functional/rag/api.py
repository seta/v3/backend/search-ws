from flask.testing import FlaskClient
from tests.infrastructure.helpers.util import auth_headers

API__ROOT = "/seta-search/rag/v1"


def get_collections(client: FlaskClient, access_token: str):
    """Get available collections."""

    url = f"{API__ROOT}/collections"

    return client.get(
        url, content_type="application/json", headers=auth_headers(access_token)
    )


def get_info(client: FlaskClient):
    """Get information about the RAG model."""

    url = f"{API__ROOT}/info"

    return client.get(url, content_type="application/json")


def query(
    client: FlaskClient,
    access_token: str,
    rag_query: str,
    max_chars: int = 20000,
    collections: list[str] = None,
    filters: dict = None,
):
    """Query the RAG model."""

    url = f"{API__ROOT}/query"

    json_data = {"query": rag_query, "max_characters": max_chars}
    if collections:
        json_data["collections"] = collections
    if filters:
        json_data["filter"] = filters

    return client.post(
        url,
        content_type="application/json",
        headers=auth_headers(access_token),
        json=json_data,
    )
