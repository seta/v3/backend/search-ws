# pylint: disable=W0621, C0301, C0116
"""Seta-search testing with pytest.

Usage:

    For testing in docker run: 
    pytest -s tests/ --settings=LOCAL
"""

import os
import configparser
from pathlib import Path
import pytest


from search.factory import create_app

from tests.test_config import TestConfig
from tests.infrastructure.init.postgresql import PostgDbTest, PostgDBConnection
from tests.infrastructure.init.es import SetaES
from tests.infrastructure.helpers.util import generate_rsa_pair
from tests.infrastructure.helpers.data_loader import load_users_data


def pytest_addoption(parser):
    """Command line options."""

    parser.addoption(
        "--settings",
        action="store",
        default="LOCAL",
        help="Section name from 'test.conf' file. LOCAL, REMOTE",
    )


@pytest.fixture(scope="session")
def settings(request):
    return request.config.getoption("--settings")


@pytest.fixture(scope="session", autouse=True)
def init_os(settings):
    """Initialize environment variables for config."""

    base_path = Path(__file__).parent
    conf_path = (base_path / "test.conf").resolve()

    config = configparser.ConfigParser()
    config.read(conf_path)
    config_section = config[settings]

    os.environ["ES_HOST"] = config_section["ES_HOST"]
    os.environ["DB_HOST"] = config_section["DB_HOST"]
    os.environ["DB_NAME"] = config_section["DB_NAME"]
    os.environ["DB_PORT"] = config_section["DB_PORT"]

    os.environ["DB_USER"] = config_section["DB_USER"]
    os.environ["DB_PASSWORD"] = config_section["DB_PASSWORD"]

    os.environ["AUTH_ROOT"] = config_section["AUTH_ROOT"]
    os.environ["NLP_ROOT"] = config_section["NLP_ROOT"]
    os.environ["ADMIN_ROOT"] = config_section["ADMIN_ROOT"]

    secrets_conf_path = (base_path / "secrets.conf").resolve()
    secrets_config = configparser.ConfigParser()
    secrets_config.read(secrets_conf_path)
    secrets_config_section = secrets_config["TEST"]

    os.environ["API_SECRET_KEY"] = secrets_config_section["API_SECRET_KEY"]
    os.environ["RAG_SECRET_KEY"] = secrets_config_section["RAG_SECRET_KEY"]

    return True


@pytest.fixture(scope="session")
def auth_root():
    return os.environ["AUTH_ROOT"]


@pytest.fixture(scope="session")
def nlp_url():
    """Root of seta-nlp web service."""

    nlp_root = os.environ["NLP_ROOT"]
    return f"http://{nlp_root}/seta-nlp/"


@pytest.fixture(scope="session")
def admin_url():
    """Root of seta-admin web service."""
    admin_root = os.environ["ADMIN_ROOT"]

    return f"http://{admin_root}/seta-admin/"


@pytest.fixture(scope="session")
def user_key_pairs():
    """Generate rsa pair for a user list."""

    user_key_pairs = {}

    data = load_users_data()

    for user in data["users"]:
        user_key_pairs[user["user_id"]] = generate_rsa_pair()

    yield user_key_pairs


@pytest.fixture(scope="session")
def test_config(auth_root, nlp_url):
    return TestConfig(auth_root=auth_root, nlp_url=nlp_url)


@pytest.fixture(scope="session")
def search_engine(test_config, admin_url):
    test_indexes = [
        "seta-test-00001",
        "seta-test-00002",
    ]  # check infrastructure/data/data_sources.json

    return SetaES(host=test_config.ES_HOST, indexes=test_indexes, admin_url=admin_url)


@pytest.fixture(scope="session")
def app(
    test_config: TestConfig,
    search_engine: SetaES,
    user_key_pairs,
):
    app = create_app(test_config)
    app.testing = True

    db_test = PostgDbTest(
        db_connection=PostgDBConnection(
            db_host=test_config.DB_HOST,
            db_port=test_config.DB_PORT,
            db_name=test_config.DB_NAME,
            db_user=test_config.DB_USER,
            db_pass=test_config.DB_PASSWORD,
        ),
        user_key_pairs=user_key_pairs,
    )

    with app.app_context():

        db_test.clear_db()
        db_test.init_db()

        search_engine.cleanup()
        search_engine.create_indexes()

        search_engine.init_es()
        search_engine.refresh_indexes()

        yield app

        # db_test.clear_db()
        # es.cleanup()


@pytest.fixture(scope="module")
def client(app):
    with app.test_client() as client:
        yield client
