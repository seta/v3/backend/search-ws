from datetime import datetime
from http import HTTPStatus
import pytz

from flask import current_app

from flask_restx import Resource

from search.apis.rag import ns
from search.apis.rag.models import info_dto as dto


@ns.rag_ns.route("info", endpoint="rag_info", methods=["GET"])
class RAGInfo(Resource):
    @ns.rag_ns.doc(
        description="Returns service current operational status.",
        responses={int(HTTPStatus.OK): "RAG status info."},
    )
    @ns.rag_ns.marshal_with(dto.info_model, mask="*")
    def get(self):
        """Service operational status."""

        now_date = datetime.now(tz=pytz.utc)
        uptime = now_date - current_app.start_time

        info = {
            "serviceName": "SeTA",
            "serviceDescription": "SeTA RAG API",
            "status": "OK",
            "version": "1.1",
            "uptime": uptime.total_seconds(),
            "timestamp": now_date,
        }

        return info
