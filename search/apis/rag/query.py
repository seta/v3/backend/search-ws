from http import HTTPStatus

from flask import current_app, abort, g
from flask_restx.errors import abort as restx_abort

from flask_restx import Resource

from search.apis.rag import ns
from search.apis.rag.models import query_dto as dto
from search.infrastructure import ApiLogicError as api_errors
from search.infrastructure.clients import nlp_client
from search.infrastructure import rag_auth_validator as rag

from search.apis.corpus.corpus_logic import corpus

from search.apis.rag.logic import query_utils as utils, parse_filters
from search.infrastructure.utils import auth_utils

MAX_DOCUMENTS = 50
MAX_QUERY_LENGTH = 100


@ns.rag_ns.route("query", endpoint="rag_query", methods=["POST"])
class RAGQuery(Resource):
    @ns.rag_ns.doc(
        description="""Search for SeTA documents based on user query.
        Filters can be applied to the search query to narrow down the search results.

        Custom filters:
        - collections: filter by a set of collections - check GET /collections
        - filter: list of {key, value} pairs            
            -- key='in_force': filter 'in force' documents, value is 'true' or 'false',
            -- key='date': search by either an interval using a list with 'operator:date' format or one specific date.
                Operators: 'gt', 'gte', 'lt' or 'lte' for greater and less than.
                Date accepts 'YYYY-MM-DD', 'YYYY-MM', 'YYYY' formats.
                Missing date components defaults to the first day of the month/year when operator is used.

                For single date, an interval is constructed to cover the entire period. For example, '2021' becomes ['gte:2021', 'lt:2022'].
        
            Examples:
            - {"filter": [{"key": "in_force", "value": "true"}]}: search for 'in force' documents
            - {"filter": [{"key": "date", "value": ["gte:2021-01-01", "lt:2022-01-01"]}]}: search for documents in 2021
            - {"filter": [{"key": "date", "value": "2022-01"}]}: search for documents in January 2022
        """,
        responses={
            int(HTTPStatus.OK): "Query response.",
            int(HTTPStatus.BAD_REQUEST): "Errors in request payload",
            int(HTTPStatus.UNAUTHORIZED): "Not authorized",
            int(HTTPStatus.INTERNAL_SERVER_ERROR): "Internal server error",
        },
        security="Bearer",
    )
    @ns.rag_ns.expect(dto.query_request_model, validate=True)
    @ns.rag_ns.marshal_with(dto.documents_response_model, skip_none=True)
    @ns.rag_ns.response(
        int(HTTPStatus.BAD_REQUEST), "", dto.errors_response_dto.error_model
    )
    @rag.rag_jwt_required()
    def post(self):
        """User query."""

        view_resources = rag.get_view_resource_permissions()
        current_app.logger.debug("View resources: %s", view_resources)

        if not view_resources:
            return []

        g.view_indices = auth_utils.get_indices(view_resources)

        request_payload: dict = ns.rag_ns.payload

        current_app.logger.debug("RAG query: %s", request_payload)

        # query_text = utils.clear_text(request_payload["query"])
        query_text = request_payload["query"]

        current_app.logger.debug("RAG query text: %s", query_text)

        collections_filter = request_payload.get("collections", None)
        if collections_filter:
            sources, collection, reference = parse_filters.parse_collections_filter(
                view_resources=view_resources, collections_filter=collections_filter
            )

            current_app.logger.debug(
                "sources=%s, collection=%s, reference=%s",
                sources,
                collection,
                reference,
            )
        else:
            sources = list({r["id"] for r in view_resources})
            collection = None
            reference = None

        if not sources:
            return []

        rag_answers = []
        try:
            in_force, date_range = parse_filters.parse_filter(
                request_payload.get("filter", None)
            )

            embeddings = nlp_client.compute_embeddings(
                text=query_text,
                api_root_url=current_app.config.get("NLP_API_ROOT_URL"),
            )

            emb_vector_list = utils.strip_embeddings(embeddings)

            if in_force is False:
                in_force = None

            documents = corpus(
                term=None,
                n_docs=MAX_QUERY_LENGTH,
                from_doc=0,
                sources=sources,
                collection=collection,
                reference=reference,
                in_force=in_force,
                sort=None,
                taxonomy=None,
                semantic_sort_id_list=[],
                emb_vector_list=emb_vector_list,
                author=None,
                date_range=date_range,
                aggs=None,
                search_type="ALL_CHUNKS_SEARCH",
                other=None,
                annotation=None,
                current_app=current_app,
            )

            current_app.logger.debug(
                "Corpus total documents: %s", documents["total_docs"]
            )

            rag_answers = utils.parse_documents(documents, MAX_DOCUMENTS)

        except api_errors.RagApiError as ae:
            restx_abort(
                HTTPStatus.BAD_REQUEST,
                message=ae.message,
                errors=ae.errors,
            )

        except api_errors.ForbiddenResourceError as fre:
            restx_abort(
                HTTPStatus.UNAUTHORIZED,
                message=fre.message,
            )

        except api_errors.ApiLogicError as ale:
            abort(
                HTTPStatus.INTERNAL_SERVER_ERROR,
                description=str(ale),
            )

        except Exception as e:
            current_app.logger.exception("Error querying RAG: %s", e)
            return abort(
                HTTPStatus.INTERNAL_SERVER_ERROR, description="Something went wrong."
            )

        response_documents = utils.prepare_chunked_response(
            rag_answers, request_payload.get("max_characters", 0)
        )
        current_app.logger.debug(
            "Response documents: %s", len(response_documents["documents"])
        )

        return response_documents


# Path: search-ws/search/apis/rag/models/query_dto.py
