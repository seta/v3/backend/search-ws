from http import HTTPStatus

from flask import g
from flask_restx import Resource

from search.apis.rag import ns
from search.apis.rag.models import collections_dto as dto
from search.infrastructure import rag_auth_validator as rag
from search.apis.rag.logic import query_utils as utils

from search.infrastructure.utils import auth_utils


@ns.rag_ns.route("collections", endpoint="rag_collections", methods=["GET"])
class RAGCollections(Resource):
    @ns.rag_ns.doc(
        description="Returns all collections.",
        responses={int(HTTPStatus.OK): "Collections."},
        security="Bearer",
    )
    @ns.rag_ns.marshal_with(dto.collections_model, mask="*")
    @rag.rag_jwt_required()
    def get(self):
        """Service collections."""

        collections = []

        view_resources = rag.get_view_resource_permissions()

        if not view_resources:
            return {"collections": collections}

        # g.view_indices is used in opensearch_client.py
        g.view_indices = auth_utils.get_indices(view_resources)
        resource_ids = list({r["id"] for r in view_resources})

        cached_collections = utils.get_aggregations_cache(sources=resource_ids)
        for collection in cached_collections.collections.values():
            if collection.key not in collections:
                collections.append(collection.key)

        collections.sort()
        return {"collections": collections}
