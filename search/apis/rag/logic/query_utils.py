"""
This module contains utility functions for RAG query.
"""

import hashlib

from search.infrastructure.extensions import cache
from search.apis.rag.models import aggs_cache
from search.apis.corpus.corpus_aggs_logic import corpus_aggregations


def strip_embeddings(embeddings: dict):
    """Get vectors from embeddings."""

    embs = []

    for emb in embeddings["emb_with_chunk_text"]:
        embs.append(emb["vector"])

    return embs


def prepare_document(doc: dict) -> dict:
    """Prepare document for RAG response."""

    hash_id = hashlib.md5(doc["chunk_text"].encode()).hexdigest()

    return {
        "hash_id": hash_id,
        "document_content": doc["chunk_text"],
        "score": doc["score"],
        "metadata": {
            "title": doc["title"],
            "date": doc["date"],
            "fragment": True,
            "authors": doc["author"],
            "language": doc["language"],
            "type": None,
            "abstract": doc["abstract"],
            "id": doc["id"],
            "url": doc["link_origin"],
            "fragment_location": None,
        },
    }


def prepare_chunked_response(documents: list[dict], max_characters: int) -> list[dict]:
    """Prepare RAG response."""

    response_docs = []
    overall_length = 0

    for doc in documents:
        document_content = doc["document_content"]
        overall_length += len(document_content)

        if overall_length > max_characters:
            break

        response_docs.append(doc)

    return {"documents": response_docs}


@cache.memoize(timeout=300)
def get_aggregations_cache(sources: list[str]) -> aggs_cache.AggregationsCache:
    """Get aggregations cache."""

    cache_result = aggs_cache.AggregationsCache()

    if not sources:
        return cache_result

    documents = corpus_aggregations(
        term=None,
        sources=sources,
        collection=None,
        reference=None,
        in_force=None,
        sort=None,
        taxonomy=None,
        semantic_sort_id_list=None,
        emb_vector_list=None,
        author=None,
        date_range=None,
        aggs=["source_collection_reference"],
        search_type=None,
        other=None,
        annotation=None,
    )

    if documents and "aggregations" in documents:
        source_collection_reference = documents["aggregations"].get(
            "source_collection_reference"
        )

        if source_collection_reference and "sources" in source_collection_reference:
            for source in source_collection_reference["sources"]:
                _parse_aggs(cache_result, source)

    return cache_result


def _parse_aggs(cache_result: aggs_cache.AggregationsCache, source: dict):
    source_key = source["key"]
    cache_result.sources.append(source_key)

    if "collections" in source:
        for collection in source["collections"]:
            collection_key = collection["key"]
            cache_result.collections[collection_key.lower()] = (
                aggs_cache.CollectionEntry(key=collection_key, source=source_key)
            )

            if "references" in collection:
                for reference in collection["references"]:
                    reference_key = reference["key"]
                    cache_result.references[reference_key.lower()] = (
                        aggs_cache.ReferenceEntry(
                            key=reference_key,
                            source=source_key,
                            collection=collection_key,
                        )
                    )


def parse_documents(documents: dict, max_limit: int):
    """Parse documents."""

    rag_answers = []

    if documents and "documents" in documents and documents["documents"]:
        for doc in documents["documents"]:
            if len(rag_answers) >= max_limit:
                break

            rag_doc = prepare_document(doc)

            doc_duplication = False
            for existing_doc in rag_answers[::-1]:
                if existing_doc["hash_id"] == rag_doc["hash_id"]:
                    doc_duplication = True
                    break

            if not doc_duplication:
                rag_answers.append(rag_doc)

    return rag_answers
