import re
from datetime import datetime as dt
from search.infrastructure import ApiLogicError as errors
from search.apis.rag.logic import query_utils as utils
from search.apis.rag.models import aggs_cache


def parse_collections_filter(
    view_resources: list[dict],
    collections_filter: list[str],
) -> tuple[list[str], list[str], list[str]]:
    """Parse collections filter."""
    resource_ids = [] if not view_resources else list({r["id"] for r in view_resources})

    cache_result = utils.get_aggregations_cache(sources=resource_ids)

    sources = []
    collections = []
    references = []

    for entry in collections_filter:
        lower_entry = entry.lower()

        if entry in cache_result.sources and entry in resource_ids:
            sources.append(entry)

        col: aggs_cache.CollectionEntry | None = cache_result.collections.get(
            lower_entry, None
        )
        if col is not None and col.source in resource_ids:

            if col.source not in sources:
                sources.append(col.source)

            if col.key not in collections:
                collections.append(col.key)

        ref: aggs_cache.ReferenceEntry | None = cache_result.references.get(
            lower_entry, None
        )
        if ref is not None and ref.source in resource_ids:

            if ref.source not in sources:
                sources.append(ref.source)

            if ref.collection not in collections:
                collections.append(ref.collection)

            if entry not in references:
                references.append(ref.key)

    return sources, collections, references


def parse_filter(query_filter: list[dict]) -> tuple[bool, list[str]]:
    """Parse query filter.

    Returns:
        tuple[bool, list[str]]: In force and date range.
    """

    if query_filter is None:
        return None, None

    in_force = None
    date_range = None

    index = 0
    for entry in query_filter:
        if "key" not in entry:
            raise errors.RagApiError(
                message="Filter key is required.",
                errors={f"filter.{index}.key": "Required."},
            )

        if "value" not in entry:
            raise errors.RagApiError(
                message="Filter key is required.",
                errors={f"filter.{index}.value": "Required."},
            )

        key = entry["key"]

        if key == "in_force":
            in_force_value = entry["value"]

            if (
                not isinstance(in_force_value, str)
                or _parse_bool(in_force_value) is None
            ):
                raise errors.RagApiError(
                    message="Filter in_force value must be one of 'true', 'false' or 'unk'.",
                    errors={
                        "key.in_force": "Invalid value, must be one of 'true', 'false' or 'unk'."
                    },
                )
            else:
                in_force = _parse_bool(in_force_value)
        elif key == "date":
            date_range = _parse_date_range(entry["value"])

        index += 1

    return in_force, date_range


def _parse_bool(value: str) -> bool | None:
    """Parse boolean value."""

    if value.lower() == "true":
        return True

    if value.lower() == "false":
        return False

    if value.lower() == "unk":
        return False

    return None


def _parse_date_range(date_range_value) -> list[str]:
    """Parse date range value."""

    pattern = r"^((gte|gt|lt|lte):)?[0-9]{4}(-[0-9]{2}(-[0-9]{2})?)?$"

    date_range = None
    if isinstance(date_range_value, list):
        date_range = date_range_value
    elif isinstance(date_range_value, str):
        date_range = [date_range_value]

    if date_range is None:
        return None

    for dr in date_range:
        print(f"Date range: {dr}")

        if not re.match(pattern, dr):
            raise errors.RagApiError(
                message=f"Invalid date range value: {dr}, must match {pattern} pattern.",
                errors={f"filter.date.{dr}": "Invalid format."},
            )

        if not _validate_date_string(dr):
            raise errors.RagApiError(
                message=f"Invalid date range value: {dr}.",
                errors={f"filter.date.{dr}": "Invalid format."},
            )

    return date_range


def _validate_date_string(date_str: str) -> bool:
    """Validate date string."""

    if ":" in date_str:
        date_str = date_str.split(":")[1]

    # Check if the date string matches one of the allowed formats
    allowed_formats = ["%Y", "%Y-%m", "%Y-%m-%d"]
    for fmt in allowed_formats:
        try:
            dt.strptime(date_str, fmt)
            return True
        except ValueError:
            continue
    return False
