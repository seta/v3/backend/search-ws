from flask_restx import Model, fields

info_model = Model(
    "Info",
    {
        "serviceName": fields.String(
            description="The name or identifier of the service"
        ),
        "serviceDescription": fields.String(
            description="A brief description of the service's purpose and capabilities."
        ),
        "version": fields.String(description="The version of the service"),
        "status": fields.String(
            description="The status of the service", enum=["OK", "DOWN", "DEGRADED"]
        ),
        "uptime": fields.Integer(
            description="The amount of time in seconds the service has been running since the last restart."
        ),
        "timestamp": fields.DateTime(
            description="The time at which the information was generated."
        ),
    },
)

ns_models = {}
ns_models[info_model.name] = info_model
