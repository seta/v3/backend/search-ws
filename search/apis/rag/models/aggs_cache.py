class AggregationsCache:
    sources: list[str] = []
    collections: dict = {}  # {lower_case_key: CollectionEntry}
    references: dict = {}  # {lower_case_key: ReferenceEntry}


class CollectionEntry:

    key: str
    source: str

    def __init__(self, key: str, source: str):
        self.key = key
        self.source = source


class ReferenceEntry:
    key: str
    source: str
    collection: str

    def __init__(self, key: str, source: str, collection: str):
        self.key = key
        self.source = source
        self.collection = collection
