from flask_restx import Model, fields
from search.infrastructure import errors_response_dto


class StringOrListElement(fields.Raw):
    __schema_type__ = ["string", "list"]

    def output(self, key, obj, **kwargs):
        if isinstance(obj, str):
            if key == "name":
                return obj
            else:
                return "default_value"
        return super().output(key, obj, **kwargs)

    def schema(self):
        schema_dict = super().schema()
        schema_dict.pop("type")
        schema_dict["oneOf"] = [{"type": "string"}, {"type": "list"}]
        return schema_dict


ws = fields.Wildcard(fields.String)
filter_model = Model(
    "Filter",
    {
        "*": ws,
    },
)

query_request_model = Model(
    "QueryRequest",
    {
        "query": fields.String(
            required=True, description="All the context of the chat (summarised)."
        ),
        "raw_query": fields.String(description="Last query from the user."),
        "max_characters": fields.Integer(
            description="Maximum number of characters to be returned in the answers."
        ),
        "chat_id": fields.String(description="Chat identifier (UUID4)."),
        "collections": fields.List(
            fields.String(description="List of collections to search.")
        ),
        "filter": fields.List(
            fields.Nested(
                filter_model,
            ),
            example=[
                {"key": "in_force", "value": "true"},
                {"key": "date", "value": ["gte:2021-01-01", "lte:2021-02"]},
                {"key": "date", "value": "2020"},
            ],
        ),
    },
)

metadata_model = Model(
    "Metadata",
    {
        "title": fields.String(),
        "date": fields.String(),
        "fragment": fields.Boolean(),
        "authors": fields.String(),
        "language": fields.String(),
        "type": fields.String(),
        "abstract": fields.String(),
        "id": fields.String(),
        "url": fields.String(),
        "fragment_location": fields.String(),
    },
)
query_response_model = Model(
    "QueryResponse",
    {
        "metadata": fields.Nested(metadata_model),
        "document_content": fields.String(description="The content of the document."),
        "score": fields.Float(description="The score of the document."),
    },
)

documents_response_model = Model(
    "Documents", {"documents": fields.List(fields.Nested(query_response_model))}
)

ns_models = {
    filter_model.name: filter_model,
    query_request_model.name: query_request_model,
    metadata_model.name: metadata_model,
    documents_response_model.name: documents_response_model,
    query_response_model.name: query_response_model,
    errors_response_dto.error_fields_model.name: errors_response_dto.error_fields_model,
    errors_response_dto.error_model.name: errors_response_dto.error_model,
}
