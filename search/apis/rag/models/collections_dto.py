from flask_restx import Model, fields

collections_model = Model(
    "Collections",
    {"collections": fields.List(fields.String(description="The name of a collection"))},
)

ns_models = {}
ns_models[collections_model.name] = collections_model
