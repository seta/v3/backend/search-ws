from flask_restx import Api
from flask import Blueprint

from search.apis.rag.info import *
from search.apis.rag.query import *
from search.apis.rag.collections import *
from search.apis.rag.ns import *

authorizations = {
    "Bearer": {
        "type": "apiKey",
        "in": "header",
        "name": "Authorization",
        "description": "Type in the *'Value'* input box below: **'Bearer &lt;JWT&gt;'**, where JWT is the token",
    }
}

rag_bp_v1 = Blueprint("rag-api", __name__, url_prefix="/seta-search/rag/v1")
rag_api = Api(
    rag_bp_v1,
    title="RAG API",
    version="1.0",
    description="Retrieval-Augmented Generation (RAG) web service.",
    default_swagger_filename="/swagger_rag.json",
    doc="/doc",
    authorizations=authorizations,
)

rag_api.add_namespace(rag_ns, path="/")
