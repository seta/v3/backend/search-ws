from flask_restx import Namespace
from search.apis.rag.models import info_dto, query_dto, collections_dto

rag_ns = Namespace(
    "RAG",
    validate=False,
    description="Retrieval-Augmented Generation.",
)
rag_ns.models.update(info_dto.ns_models)
rag_ns.models.update(query_dto.ns_models)
rag_ns.models.update(collections_dto.ns_models)
