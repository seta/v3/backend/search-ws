import os
from flask import current_app
from nltk.tokenize import word_tokenize

from search.infrastructure.clients import opensearch_client
from search.infrastructure.ApiLogicError import ApiLogicError

from .corpus_build import (
    build_search_query,
    add_knn_search_query,
    fill_body_for_sort,
    fill_body_for_aggregations,
)

from .corpus_response import handle_aggs_response

OPERATORS = ["AND", "OR", "(", ")"]

STOP_WORDS = set(
    [
        "the",
        "and",
        "or",
        "but",
        "is",
        "are",
        "was",
        "were",
        "to",
        "from",
        "in",
        "on",
        "at",
        "by",
        "with",
        "as",
        "for",
        "of",
        "a",
        "an",
    ]
    + OPERATORS
)


def corpus_aggregations(
    term,
    sources,
    collection,
    reference,
    in_force,
    sort,
    taxonomy,
    semantic_sort_id_list,
    emb_vector_list,
    author,
    date_range,
    aggs,
    search_type,
    other,
    annotation,
):
    """Search for documents in the corpus and return aggregations."""

    if search_type is None or search_type not in current_app.config["SEARCH_TYPES"]:
        search_type = "CHUNK_SEARCH"

    #! This avoids performance issue when searching for chunks with empty filters
    if (
        search_type == "CHUNK_SEARCH"
        and not semantic_sort_id_list
        and not emb_vector_list
        and (not term or _check_all_stop_words(term))
    ):
        search_type = "DOCUMENT_SEARCH"

    body = _build_corpus_request(
        term,
        sources,
        collection,
        reference,
        in_force,
        sort,
        taxonomy,
        semantic_sort_id_list,
        emb_vector_list,
        author,
        date_range,
        aggs,
        search_type,
        other,
        annotation,
    )

    res = opensearch_client.msearch_view(body)

    documents = _handle_corpus_response(aggs, res, search_type)
    return documents


def _build_corpus_request(
    term,
    sources,
    collection,
    reference,
    in_force,
    sort,
    taxonomy,
    semantic_sort_id_list,
    emb_vector_list,
    author,
    date_range,
    aggs,
    search_type,
    other,
    annotation,
):
    query = build_search_query(
        term,
        sources,
        collection,
        reference,
        in_force,
        author,
        date_range,
        search_type,
        other,
        taxonomy,
        annotation,
    )
    body = {
        "size": 0,
        "from": 0,
        "track_total_hits": False,
        "_source": [],
    }

    if search_type == "CHUNK_SEARCH":
        body["collapse"] = {"field": "document_id"}
        body["aggs"] = {"total": {"cardinality": {"field": "document_id"}}}

    if emb_vector_list or semantic_sort_id_list:
        body = add_knn_search_query(
            current_app, emb_vector_list, semantic_sort_id_list, body, query
        )
        body["min_score"] = float(os.getenv("SIMILARITY_THRESHOLD", "0.7"))
    else:
        body["query"] = query

    body = fill_body_for_sort(body, sort)
    body = fill_body_for_aggregations(aggs, body, current_app, search_type)
    return body


def _handle_corpus_response(aggs, res, search_type):
    documents = {"aggregations": None}

    for response in res["responses"]:
        if "error" in response:
            raise ApiLogicError(f"Malformed query: {response['error']}")

        documents = handle_aggs_response(
            aggs, response, documents, current_app, search_type
        )

    return documents


def _check_all_stop_words(term: str):
    term_list = set(word_tokenize(term))
    if STOP_WORDS.intersection(term_list) == term_list:
        return True

    return False
