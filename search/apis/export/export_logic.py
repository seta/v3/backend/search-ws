# pylint: disable=missing-function-docstring

import io
import requests
import pandas as pd

from search.infrastructure.extensions import cache
from search.infrastructure.auth_validator import validate_view_permissions
from search.infrastructure.ApiLogicError import ForbiddenResourceError, ApiLogicError
from search.infrastructure.helpers import is_field_in_doc


@cache.cached(key_prefix="catalog_fields")
def get_catalog_fields_name(app, request):
    url = app.config.get("CATALOGUE_API_ROOT_URL") + "fields"
    try:
        result = requests.get(
            url=url, headers=request.headers, cookies=request.cookies, timeout=30
        )
    except Exception as e:
        raise ApiLogicError("catalog api error") from e
    names = []
    for f in result.json():
        names.append(f["name"])
    return names


def _get_doc_from_es(doc_tuple, fields, app, request, index=None):

    doc_id = doc_tuple[0]
    doc_path = doc_tuple[1]

    if index is None:
        index = app.config["INDEX"]

    source = []

    available_fields = get_catalog_fields_name(app, request)

    for field in fields:
        if field in available_fields:
            source.append(field)

    if "source" not in source:
        source.append("source")

    body = {"query": {"bool": {"must": [{"match": {"_id": doc_id}}]}}}
    response = app.es.search(index=index, body=body, _source=source, size=1)

    doc = {}
    source_id = ""

    if "error" in response:
        raise ApiLogicError("Malformed query.")

    for document in response["hits"]["hits"]:
        source_id = document["_source"]["source"]
        for f in fields:
            if f == "path":
                doc[f] = doc_path
                continue
            if f not in available_fields:
                continue
            if f == "_id":
                doc[f] = document[f]
                continue
            doc[f] = is_field_in_doc(document["_source"], f)

    return doc, source_id


def _from_json_obj_to_tuple(ids: dict) -> list:
    ids_tuple = []

    for obj in ids:
        t = (obj["id"], obj["path"], obj.get("source", ""))
        ids_tuple.append(t)

    return ids_tuple


def export(ids, fields, app, export_format, request):

    documents = []
    ids_tuple = _from_json_obj_to_tuple(ids)
    unique_ids = set(ids_tuple[: app.config["EXPORT_DOCUMENT_LIMIT"]])

    validated = {}
    forbidden = []

    def _validate_source_id(sid) -> str | None:
        nonlocal validated, forbidden

        if sid in forbidden:
            return None

        if validated.get(sid) is not None:
            return validated[sid]
        else:
            try:
                view_resources = validate_view_permissions(sources=[sid])
                index = view_resources[0].get("index")
                validated[sid] = index

                return index
            except ForbiddenResourceError:
                forbidden.append(sid)
                return None
            except Exception as e:
                raise ApiLogicError("export error") from e

    for doc_tuple in unique_ids:

        index = None
        source_id = doc_tuple[2]

        if source_id is not None and source_id != "":
            index = _validate_source_id(source_id)
            if index is None:
                continue

        doc, source = _get_doc_from_es(doc_tuple, fields, app, request, index=index)

        if source_id is None or source_id == "":
            index = _validate_source_id(source)
            if index is None:
                continue

        documents.append(doc)

    if export_format == "text/csv":
        df = pd.DataFrame(documents)
        csv_buffer = io.StringIO()
        df.to_csv(csv_buffer, index=False)
        csv_output = csv_buffer.getvalue()
        csv_buffer.close()
        return csv_output

    if export_format == "application/json":
        return {"documents": documents}
