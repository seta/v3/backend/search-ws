# pylint: disable=protected-access

from functools import wraps
from flask import current_app, g

from flask_jwt_extended import view_decorators, tokens
from flask_jwt_extended.exceptions import NoAuthorizationError
from flask_jwt_extended.config import config as jwt_config

from search.infrastructure.clients import auth_client
from search.infrastructure.models.discovery_domain import DiscoveryDomainEnum


def _decode_token(
    encoded_token: str, csrf_value=None, allow_expired: bool = False
) -> dict:
    secret = current_app.config["RAG_SECRET_KEY"]

    kwargs = {
        "algorithms": jwt_config.decode_algorithms,
        "audience": jwt_config.decode_audience,
        "csrf_value": csrf_value,
        "encoded_token": encoded_token,
        "identity_claim_key": jwt_config.identity_claim_key,
        "issuer": jwt_config.decode_issuer,
        "leeway": jwt_config.leeway,
        "secret": secret,
        "verify_aud": jwt_config.decode_audience is not None,
    }

    return tokens._decode_jwt(**kwargs, allow_expired=allow_expired)


def _decode_jwt_from_request() -> dict:
    """Ensure that the request has a valid JWT token."""

    encoded_token, _ = view_decorators._decode_jwt_from_headers()
    decoded_token = _decode_token(encoded_token)

    return decoded_token


def verify_rag_jwt_in_request():
    """Ensure that the request has a valid JWT token and store it in the global context."""

    try:
        decoded_token = _decode_jwt_from_request()
        g._rag_jwt = decoded_token
    except Exception as e:
        raise NoAuthorizationError from e


def get_jwt() -> dict | None:
    """Get the current JWT object."""
    decoded_jwt = g.get("_rag_jwt", None)

    if decoded_jwt is None:
        raise RuntimeError(
            "You must call `@rag_jwt_required()` or `verify_rag_jwt_in_request()` "
            "before using this method"
        )

    return decoded_jwt


def get_view_resource_permissions() -> list[str]:
    """Gets a list of data source view permissions from the user."""

    jwt = get_jwt()
    user_id = jwt.get("sub", None)

    if user_id is not None:
        try:
            data = auth_client.get_data_source_permissions(
                user_id=user_id, domain=DiscoveryDomainEnum.RAG
            )

            permissions: dict = data.get("permissions", None)
            if permissions and "view" in permissions:
                return permissions["view"]

            return None
        except Exception:
            current_app.logger.exception("Failed to get data source permissions")

    return None


def rag_jwt_required():
    """A decorator that checks the JWT token for RAG authorization."""

    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):

            verify_rag_jwt_in_request()

            return fn(*args, **kwargs)

        return decorator

    return wrapper
