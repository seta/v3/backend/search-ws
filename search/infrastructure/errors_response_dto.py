from flask_restx import Model, fields

error_message = fields.Wildcard(fields.String)
error_fields_model = Model("ErrorFields", {"*": error_message})
error_model = Model(
    "BadRequestMessage",
    {
        "message": fields.String(description="Error message"),
        "errors": fields.Nested(error_fields_model),
    },
)
