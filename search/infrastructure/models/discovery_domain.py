from enum import Enum


class DiscoveryDomainEnum(str, Enum):
    SEARCH = "search"
    RAG = "rag"
    NLP = "nlp"
