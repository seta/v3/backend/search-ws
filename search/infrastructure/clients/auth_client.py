import urllib.parse as urllib_parse
import requests

from flask import current_app
from search.infrastructure.extensions import cache


@cache.memoize()
def get_data_source_permissions(user_id: str, domain: str = None) -> dict:
    """Get user data source permissions from Authorization service."""

    api_root = current_app.config["INTERNAL_AUTHORIZATION_API"]

    url = urllib_parse.urljoin(api_root, f"data-sources/{user_id}")

    if domain:
        url = urllib_parse.urljoin(url, f"?domain={domain}")

    result = requests.get(
        url=url, headers={"Content-Type": "application/json"}, timeout=30
    )
    result.raise_for_status()

    return result.json()
