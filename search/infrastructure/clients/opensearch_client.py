import json
from flask import current_app, g

from search.infrastructure.auth_validator import get_resource_permissions
from search.infrastructure.utils import auth_utils


def msearch_view(body: dict) -> dict:
    """
    Search for multiple indices with view permissions.
    """

    if "view_indices" not in g:
        try:
            perms = get_resource_permissions("view")
            g.view_indices = auth_utils.get_indices(perms)
        except Exception:
            return {}

    return _msearch(body, g.view_indices)


def msearch_ownership(body: dict) -> dict:
    """
    Search for multiple indices with ownership permissions.
    """

    if "ownership_indices" not in g:
        try:
            perms = get_resource_permissions("ownership")
            g.ownership_indices = auth_utils.get_indices(perms)
        except Exception:
            return {}

    return _msearch(body=body, indices=g.ownership_indices)


def _msearch(body: dict, indices: list[str]) -> dict:
    """
    Search for multiple indices
    """

    request = _compose_request(body=body, indices=indices)

    current_app.logger.debug(f"msearch request: {request}")

    return current_app.es.msearch(body=request)


def _compose_request(body, indices):

    if not indices:
        indices = [current_app.config["INDEX"][0]]

    search_arr = [{"index": indices}, body]
    request = ""
    for each in search_arr:
        request += f"{json.dumps(each)} \n"
    return request
