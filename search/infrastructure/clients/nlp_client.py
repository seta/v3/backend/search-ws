from io import BytesIO
import urllib.parse as urllib_parse
import requests
from flask import Request, current_app

from search.infrastructure.ApiLogicError import ApiLogicError


def compute_concordance(abstract: str, term: str, text: str, api_root_url: str) -> dict:
    """Compute concordance for a given term in a text."""

    url = urllib_parse.urljoin(api_root_url, "internal/compute_concordance")
    data = {"term": term, "abstract": abstract, "text": text}

    try:
        result = requests.post(url=url, json=data, timeout=600)
        result.raise_for_status()
        return result.json()
    except Exception as e:
        raise ApiLogicError("nlp compute_concordance api error") from e


def compute_embeddings(text: str, api_root_url: str) -> dict:
    """Compute embeddings."""

    url = urllib_parse.urljoin(api_root_url, "internal/compute_embeddings")
    data = {"text": text}

    try:
        result = requests.post(
            url=url,
            json=data,
            timeout=600,
        )
        result.raise_for_status()

        return result.json()
    except Exception as e:
        raise ApiLogicError("nlp compute_embeddings api error") from e


def compute_embedding(chunk_text: str, request: Request, api_root_url: str) -> dict:
    """Compute embedding."""

    url = urllib_parse.urljoin(api_root_url, "compute_embedding")
    data = {"text": chunk_text}

    try:
        result = requests.post(
            url=url,
            headers=request.headers,
            cookies=request.cookies,
            json=data,
            timeout=600,
        )
        result.raise_for_status()

        return result.json()
    except Exception as e:
        raise ApiLogicError("nlp compute_embedding api error") from e


def parse_file(file: BytesIO, request: Request, api_root_url: str) -> str:
    """Parse text from file."""

    url = urllib_parse.urljoin(api_root_url, "/file_to_text")
    file.seek(0)

    headers = dict(request.headers)
    if "Content-Length" in headers:
        headers.pop("Content-Length")
    headers["Accept"] = "text/plain"

    try:
        result = requests.post(
            url=url,
            headers=request.headers,
            cookies=request.cookies,
            files={"file": file},
            timeout=600,
        )
        result.raise_for_status()

        return result.text
    except Exception as e:
        current_app.logger.exception("nlp parse_file api error")
        raise ApiLogicError("nlp parse_text api error") from e
