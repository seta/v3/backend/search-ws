"""Initialize app extensions."""

import os
from flask_jwt_extended import JWTManager
from flask_caching import Cache
from search.infrastructure.log_setup import LogSetup

cache_timeout = int(os.environ.get("CACHE_TIMEOUT", "300"))

jwt = JWTManager()
logs = LogSetup()
cache = Cache(
    config={"CACHE_TYPE": "SimpleCache", "CACHE_DEFAULT_TIMEOUT": cache_timeout}
)
