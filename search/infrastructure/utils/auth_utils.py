from flask import current_app


def get_indices(view_permissions: list[dict]) -> list[str]:
    """Builds and validates view indices list."""

    indices = []

    for index in {p["index"] for p in view_permissions}:

        if index in indices:
            continue

        if current_app.es.indices.exists(index=index):
            indices.append(index)

    return indices
