from functools import wraps
from flask_jwt_extended import get_jwt, verify_jwt_in_request
from flask import jsonify, current_app

from search.infrastructure.ApiLogicError import ForbiddenResourceError
from search.infrastructure.clients.auth_client import get_data_source_permissions
from search.infrastructure.models.discovery_domain import DiscoveryDomainEnum


def auth_validator(role=None):
    """A decorator that checks the JWT token for role authorization."""

    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            verify_jwt_in_request()

            if role is not None:
                claims = get_jwt()

                if claims.get("role", None) != role:
                    response = jsonify({"message": "Unauthorized access"})
                    response.status_code = 403
                    return response

            return fn(*args, **kwargs)

        return decorator

    return wrapper


def validate_view_permissions(sources: list[str]) -> list[dict]:
    """
    Verify that all resources in the `sources` are contained by the view array of the decoded token.
    Return the list of view resources if `sources` are empty
    """

    view_resources = get_resource_permissions(permission="view")
    if not view_resources:
        raise ForbiddenResourceError(resource_id=None)

    # restrict query only to view_resources
    if not sources:
        return view_resources

    result = []
    for s in sources:
        found = False
        for r in view_resources:
            if r["id"].lower() == s.lower():
                result.append(r)
                found = True
                break
        if not found:
            raise ForbiddenResourceError(resource_id=s)

    return result


def get_ownership(source: str) -> dict | None:
    """
    Validate that the `source` is contained by the resource_permissions.ownership array of the decoded token
    """

    return _validate_source_permission(
        source=source, permission="ownership", domain=None
    )


def is_administrator() -> bool:
    """
    Validate that the user is an administrator
    """

    jwt = get_jwt()
    user_id = jwt.get("sub", None)

    if user_id is not None:
        resources = get_resource_permissions(permission="administrator")
        if resources is not None and len(resources) > 0:
            return True

    return False


def get_resource_permissions(
    permission: str,
    domain: str = DiscoveryDomainEnum.SEARCH,
) -> list[dict]:
    """Gets list of data source permissions for the user.
    Returns a list of {id: str, index: str}.

    :param permission:
        One of 'view', 'ownership', 'administrator'.
    """

    jwt = get_jwt()
    user_id = jwt.get("sub", None)

    if user_id is not None:
        try:
            resources = get_data_source_permissions(user_id, domain=domain)

            permissions = resources.get("permissions", None)
            if permissions:
                return permissions.get(permission, None)

            return None
        except Exception:
            current_app.logger.exception("Failed to get data source permissions")

    return None


def _validate_source_permission(
    source: str, permission: str, domain: str = DiscoveryDomainEnum.SEARCH
) -> dict | None:
    if source is None:
        return None

    resources = get_resource_permissions(permission, domain=domain)
    if resources is not None:
        for r in resources:
            if r["id"].lower() == source.lower():
                return r

    return None
