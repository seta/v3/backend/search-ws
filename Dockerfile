FROM python:3.10

RUN useradd seta 
ARG ROOT=/home/seta

WORKDIR $ROOT

# set env variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt ./requirements.txt
RUN pip install --no-cache-dir -r requirements.txt
RUN python3 -m nltk.downloader punkt

#copy configuration files
COPY ./config/search.conf /etc/seta/
COPY ./config/logs.conf /etc/seta/

COPY ./*.py ./
COPY ./search ./search

CMD ["gunicorn", "--conf", "/home/seta/gunicorn_conf.py", "--bind", "0.0.0.0:8081", "--capture-output" , "--access-logformat", "[seta.search] %(h)s %(l)s %(u)s %(t)s. %(r)s. %(s)s %(b)s. %(f)s. %(a)s", "--chdir", "/home/seta", "app:app"]